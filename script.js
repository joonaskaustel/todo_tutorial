//Check off specific todos by clicking
$("ul").on("click", "li", function(){
    $(this).toggleClass("completed");
});

//click on x to delete a todo
$("ul").on("click", "span", function (event) { 
    $(this).parent().fadeOut(500, function(){
        $(this).remove();
    });
    event.stopPropagation();
});

$("#newTodo").keypress(function (event) { 
    if(event.which === 13){
        //grab a new todo from input
        var newItem = $(this).val();
        //create a new li and add to ul
        $("ul").append("<li><span><i class='fa fa-trash'></i></span>" + newItem + "</li>");
    }
});
